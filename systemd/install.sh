#!/usr/bin/env bash

# set variable
WORKD="/etc/fwsync/systemd"
SYSTEMD="/usr/lib/systemd/system"
WHICH="/usr/bin/which"
CP=`$WHICH cp`
SYSTEMCTL=`$WHICH systemctl`
SYSCTL=`$WHICH sysctl`
YUM=`$WHICH yum`

# stop and disable firewalld service
$SYSTEMCTL disable firewalld.service
$SYSTEMCTL stop firewalld.service

# if iptables-services not installed then install it
$YUM list installed iptables-services
if [[ $? -eq 1 ]]
then
  $YUM install iptables-services -y
fi

# if rsync not installed then install it
$YUM list installed rsync
if [[ $? -eq 1 ]]
then
  $YUM install rsync -y
fi

# ... then stop and disable it
$SYSTEMCTL disable iptables.service
$SYSTEMCTL stop iptables.service

# then copy own services and timer files
$CP $WORKD/*.{service,timer} $SYSTEMD/
$SYSTEMCTL daemon-reload

# then enable and start it
$SYSTEMCTL enable fwiptables.service fwsync.service fwsync.timer
$SYSTEMCTL start fwiptables.service fwsync.service fwsync.timer

# disable IPv6 proto
$CP $WORKD/90-disable-ipv6.conf /etc/sysctl.d/
$SYSCTL -p

