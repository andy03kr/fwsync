#!/usr/bin/env bash

# main config file
source /etc/fwsync/fwsync.conf

$ECHO "================== start fwsync.sh in $($DATE +'%d.%m.%Y') $CTIME ==================" >> $LOG

# sync master.israelnumber.com:/etc/fwsync/ exclude dir from $RSYNCEXCLUDE variable
$RSYNC -a --delete $RSYNCEXCLUDE -e "$SSH -i $SSHKEY -o StrictHostKeyChecking=no" $SSHUSER@$SSHHOST:$FWSYNCD/ $FWSYNCD/ >> $LOG

# sync master rule by hostname
$RSYNC -a --delete -e "$SSH -i $SSHKEY -o StrictHostKeyChecking=no" $SSHUSER@$SSHHOST:$REPOD/$($HOSTNAME)/ $TMPD/ >> $LOG

# remove old or unused files from ipset.d dir
for CURRFILE in $($LS $IPSETD/)
do
  if [[ ! -f "$TMPD/ipset.d/$CURRFILE" ]]
  then
    $ECHO "remove $IPSETD/$CURRFILE" >> $LOG
    $RM -f $IPSETD/$CURRFILE >> $LOG
  fi
done

# remove old unused files from iptables.d dir
for CURRFILE in $($LS $IPTABLESD/)
do
  if [[ ! -f "$TMPD/iptables.d/$CURRFILE" ]]
  then
    $ECHO "remove $IPTABLESD/$CURRFILE" >> $LOG
    $RM -f $IPTABLESD/$CURRFILE >> $LOG
  fi
done

# get md5sum for every file in directory /etc/fwsync/ipset.d/ into array $TLIST
TLIST=($($MD5SUM $TMPD/ipset.d/*))

# in loop enumerate elements of array...
for (( i = 0; i <= ${#TLIST[@]}-1; i += 2 ))
do
  # then get file name
  CURRFILE=$(${BASENAME} ${TLIST[$i+1]})
  # if file not found in /etc/fwsync/ipset.d/ or md5sum not equivalent to same file in /etc/fwsync/tmp/ipset.d/ ...
  if [[ ! -f "$IPSETD/$CURRFILE" || "${TLIST[$i]}" != "$($MD5SUM $IPSETD/$CURRFILE | $AWK '{print $1}')" ]]
  then
    # means file will be changed from master server repo and must be updayted localy
    # here copy file from /etc/fwsync/tmp/ipset.d/ to /etc/fwsync/ipset.d/
    # then load changed list into ipset
    $ECHO "file $CURRFILE will be changed" >> $LOG
    $CP -f ${TLIST[$i+1]} $IPSETD/ >> $LOG
    $ECHO "ipset list $IPSETD/$CURRFILE are updated" >> $LOG
    $IPSET --exist restore -f $IPSETD/$CURRFILE >> $LOG
  fi
done

# get md5sum for every file in directory /etc/fwsync/iptables.d/ into array $TLIST
TLIST=($($MD5SUM $TMPD/iptables.d/*))

# in loop enumerate elements of array...
for (( i = 0; i <= ${#TLIST[@]}-1; i += 2 ))
do
  # then get file name
  CURRFILE=`${BASENAME} ${TLIST[$i+1]}`
  # if file not found in /etc/fwsync/iptables.d/ or md5sum not equivalent to same file in /etc/fwsync/tmp/iptables.d/ ...
  if [[ ! -f "$IPTABLESD/$CURRFILE" || "${TLIST[$i]}" != "$($MD5SUM $IPTABLESD/$CURRFILE | $AWK '{print $1}')" ]]
  then
    # means file will be changed in master server repo and must be updayted localy
    # here copy file from /etc/fwsync/tmp/iptables.d/ to /etc/fwsync/iptables.d/
    # then set IPTCH-flag to restart iptables
    $ECHO "file $IPTABLESD/$CURRFILE will be changed" >> $LOG
    IPTCH+=($CURRFILE)
    $CP -f ${TLIST[$i+1]} $IPTABLESD/ >> $LOG
  fi
done

# if this flag set (size of array greated then 0)
if [[ ${#IPTCH[@]} -gt 0 ]]
then
  # restart iptables service
  $ECHO "iptables rules are updated" >> $LOG
  $SYSTEMCTL restart fwiptables.service >> $LOG
fi

# logged
$ECHO "system fwsync are up ot date $CTIME" >> $LOG

