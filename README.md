# fwsync

sync iptables rules and ipset lists from master-server

tested on CentOS Linux release 7.9.2009 (Core)

master:
1)  download https://gitlab.com/andy03kr/fwsync.git to master server in /etc/fwsync/
2)  create user for sync (fwsync-user) and generate ssh key
3)  cp /home/fwsync/.ssh/id_rsa.pub /etc/fwsync/fwsync.key
4)  ipset create white-list hash:ip
5)  ipset add white-list 192.168.0.1
6)  ipset save -f /etc/fwsync/ipset.d/white-list

   add another trust IP into white-list

   create needed lists for ipset and save in /etc/fwsync/ipset.d/ directory

7)  on master side create repo-directory for client:

   mkdir -p /etc/fwsync/repo/CLIENT.NAME/ipset.d

   mkdir -p /etc/fwsync/repo/CLIENT.NAME/iptables.d

8)  cp /etc/fwsync/ipset.d/white-list /etc/fwsync/repo/slave0/ipset.d
9)  cp /etc/fwsync/iptables.d/* /etc/fwsync/repo/slave0/iptables.d/

slave*:
1) scp -r fwsync-user@master.home.lan:/etc/fwsync/ /etc/
2) create user for sync (fwsync-user)
3) run install script /etc/fwsync/systemd/install.sh
done

when master-files (repo/CLIENT.NAME/ipset.d/* or repo/CLIENT.NAME/iptables.d/*) changed - slave will apply this changes
