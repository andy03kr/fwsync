#!/usr/bin/env bash

ECHO="/usr/bin/echo"

# check config file fnd read this
if [[ -f "/etc/fwsync/fwiptables.conf" ]]
then
    source "/etc/fwsync/fwiptables.conf"
else
  $ECHO "file /etc/fwsync/fwiptables.conf not found"
  exit 1
fi

# enumerate file-lists for ipset
for CURRFILE in $($LS $IPSETD/)
do
  # then load this when start server or restart service fwiptables.service 
  $IPSET --exist restore -f $IPSETD/$CURRFILE
done

# read and load default policy and rule set for iptables
source $IPTABLESD/defaults.rule

# read and load filter table file for iptables
source $IPTABLESD/filter.rule

# if NAT-variable set to NO then computer
# not be router and NAT-rules disable
if [[ "$NAT" == "yes" ]]
  then
    source $IPTABLESD/nat.rule
fi

